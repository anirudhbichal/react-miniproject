import './App.css';
import Filter from './components/Filter';
import Products from './components/Products';
import User from './components/User';
import logo from './logo.svg';

function App() {
  return (
    <div className="App">
      <header className="header bg-white">
        <div className="container px-0 px-lg-3">
          <nav className="navbar navbar-expand-lg navbar-light py-3 px-lg-0"><a className="navbar-brand" href="index.html"><span className="font-weight-bold text-uppercase text-dark">Boutique</span></a>
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <a className="nav-link active" href="index.html">Home</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="shop.html">Shop</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="detail.html">Product detail</a>
                </li>
              </ul>
              <ul className="navbar-nav ml-auto">               
                <li className="nav-item"><i className="fas fa-user-alt mr-1 text-gray"></i>Login</li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
      <div className="container">
        <div className="py-5 container p-0 text-left">
          <div className="row">
            <Filter/>
            <Products/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
