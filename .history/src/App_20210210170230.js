import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>React Search and Filter Application</h2>
      </header>
    </div>
  );
}

export default App;
