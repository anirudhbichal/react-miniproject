import './App.css';
import User from './components/User';
import logo from './logo.svg';

function App() {
  return (
    <div className="App">
      <header className="App-header d-flex">
        <div className="d-flex">
          <img src={logo} alt="logo" width="60px" height="50px" />
        </div>
        <div className="d-flex user-info ml-auto">
          <h2>
            <User />
          </h2>
        </div>
      </header>
    </div>
  );
}

export default App;
