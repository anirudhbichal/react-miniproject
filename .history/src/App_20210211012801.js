import './App.css';
import User from './components/User';

function App() {
  return (
    <div className="App">
      <header className="App-header d-flex flex-row justify-content-end">
        <div className="d-flex">
          <h2>React Search and Filter Application</h2>
        </div>
        <div className="d-flex user-info">
          <User />
        </div>
      </header>
    </div>
  );
}

export default App;
