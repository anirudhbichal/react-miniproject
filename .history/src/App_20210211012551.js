import './App.css';
import User from './components/User';

function App() {
  return (
    <div className="App">
      <header className="App-header d-flex">
        <div className="d-flex justify-content-start align-items-start">
          <h2>React Search and Filter Application</h2>
        </div>
        <div className="d-flex user-info justify-content-end align-items-end">
          <User />
        </div>
      </header>
    </div>
  );
}

export default App;
