import './App.css';
import User from './components/User';

function App() {
  return (
    <div className="App">
      <header className="App-header d-flex">
        <h2 className="justify-content-start align-items-start">React Search and Filter Application</h2>
        <div className="user-info justify-content-end align-items-end">
          <User />
        </div>
      </header>
    </div>
  );
}

export default App;
