import './App.css';
import Filter from './components/Filter';
import Products from './components/Products';
import User from './components/User';
import logo from './logo.svg';

function App() {
  return (
    <div className="App">
      <header className="App-header d-flex align-items-center">
        <div className="d-flex align-items-center">
          <h2><img src={logo} alt="logo" width="60px" height="60px" /> E-Comm</h2>
        </div>
        <div className="d-flex user-info ml-auto">
          <h2>
            <User />
          </h2>
        </div>
      </header>
      <div className="d-flex app-body align-content-around flex-wrap">
        <div className="row">
          <div className="col-2">
            <Filter/>
          </div>
          <div>
            <Products className="col-10"/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
