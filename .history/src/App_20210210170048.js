import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>React Search and Filter Application</h1>
      </header>
    </div>
  );
}

export default App;
