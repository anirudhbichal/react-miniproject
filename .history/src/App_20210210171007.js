import './App.css';
import User from './components/User';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>React Search and Filter Application</h2>
        <div className="user-info">
          <User />
        </div>
      </header>

    </div>
  );
}

export default App;
