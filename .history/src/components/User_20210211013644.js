import React from 'react';

class User extends React.Component {

    async componentDidMount() {
        const response = await fetch("https://randomuser.me/api");
        const data = await response.json();
        console.log(data.results);
    }

    render() {
        return (
            <div><span>User Icon</span> User Name</div>
        )
    }
}

export default User