import React from 'react';

const User = (props) => {
    return (
        <>
            {props.user ? <span><img className="user-icon" src={props.user.picture.thumbnail} alt="user-icon" /> {props.user.name.first} {props.user.name.last}</span> : <span>Login</span>}
        </>
    )
}

export default User