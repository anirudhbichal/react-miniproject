import React from 'react'

class Filter extends React.Component {
    state = {
        categories : null
    }

    async componentDidMount() {
        const response = await fetch('https://fakestoreapi.com/products/categories')
        const data = await response.json();
        this.setState({
            categories: data
        });
    }

    render() {
        const {categories} = {...this.state};
        return(
            <div className="col-lg-3 order-2 order-lg-1">
                <h5 className="text-uppercase mb-4">Categories</h5>
                {(categories && categories.length > 0) ? 
                    <>
                        <div className="py-2 px-4 bg-dark text-white mb-3"><strong className="small text-uppercase font-weight-bold">Fashion &amp; Acc</strong></div>
                        <ul className="list-unstyled small text-muted pl-lg-4 font-weight-normal">
                            {categories.map((each,index) => {
                                return(
                                    <li className="mb-2" key={index}>{each}</li>
                                );
                            })}
                        </ul>
                    </> 
                : <div>Loading..</div>}
            </div>
        )
    }
}

export default Filter