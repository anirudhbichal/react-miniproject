import React from 'react';

class User extends React.Component {
    state = {
        user: null
    };

    async componentDidMount() {
        const response = await fetch("https://randomuser.me/api");
        const data = await response.json();
        this.setState({
            user: data.results[0]
        });
    }

    render() {
        return (
            <div>
                {this.state.user ? <div>{JSON.stringify(this.state.user)}</div> : null}
            </div>
        )
    }
}

export default User