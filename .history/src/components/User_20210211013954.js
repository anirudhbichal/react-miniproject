import React from 'react';

class User extends React.Component {
    state = {
        user: null
    }

    async componentDidMount() {
        const response = await fetch("https://randomuser.me/api");
        const data = await response.json();
        console.log(data.results[0]);
        this.setState({
            user: data.results[0]
        });
    }

    render() {
        return (
            <div><span><img src={this.state.user.picture.thumbnail} alt="user-icon"/></span> {this.state.user.name.first} {this.state.user.name.last}</div>
        )
    }
}

export default User