import React from 'react'

class Filter extends React.Component {
    render() {
        return(
            <div className="col-lg-3 order-2 order-lg-1">
                <h5 className="text-uppercase mb-4">Categories</h5>
                <div className="py-2 px-4 bg-dark text-white mb-3"><strong className="small text-uppercase font-weight-bold">Fashion &amp; Acc</strong></div>
                <ul className="list-unstyled small text-muted pl-lg-4 font-weight-normal">
                    <li className="mb-2">Women's T-Shirts</li>
                    <li className="mb-2">Men's T-Shirts</li>
                    <li className="mb-2">Dresses</li>
                    <li className="mb-2">Novelty socks</li>
                    <li className="mb-2">Women's sunglasses</li>
                    <li className="mb-2">Men's sunglasses</li>
                </ul>
                <div className="py-2 px-4 bg-light mb-3"><strong className="small text-uppercase font-weight-bold">Health &amp; Beauty</strong></div>
                <ul className="list-unstyled small text-muted pl-lg-4 font-weight-normal">
                    <li className="mb-2">Shavers</li>
                    <li className="mb-2">bags</li>
                    <li className="mb-2">Cosmetic</li>
                    <li className="mb-2">Nail Art</li>
                    <li className="mb-2">Skin Masks &amp; Peels</li>
                    <li className="mb-2">Korean cosmetics</li>
                </ul>
                <div className="py-2 px-4 bg-light mb-3"><strong className="small text-uppercase font-weight-bold">Electronics</strong></div>
                <ul className="list-unstyled small text-muted pl-lg-4 font-weight-normal mb-5">
                    <li className="mb-2">Electronics</li>
                    <li className="mb-2">USB Flash drives</li>
                    <li className="mb-2">Headphones</li>
                    <li className="mb-2">Portable speakers</li>
                    <li className="mb-2">Cell Phone bluetooth headsets</li>
                    <li className="mb-2">Keyboards</li>
                </ul>
            </div>
        )
    }
}

export default Filter