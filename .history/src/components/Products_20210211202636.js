import React from 'react';

class Products extends React.Component {
    state = {
        products : []
    }

    async componentDidMount() {
        const response = await fetch('https://fakestoreapi.com/products');
        const data = await response.json();
        this.setState({
            products: data
        });
    }
    
    render() {
        const {products} = {...this.state};
        return(
            <div className="col-lg-9 order-1 order-lg-2 mb-5 mb-lg-0">
                <div className="row mb-3 align-items-center">
                    <div className="col-lg-6 mb-2 mb-lg-0">
                        <p className="text-small text-muted mb-0">Showing {products.length} results</p>
                    </div>
                </div>
                {products && products.length > 0 ? 
                    <div className="row">
                        {products.map((each, index) => {
                            return (
                                <div className="col-lg-4 col-sm-6" key={index}>
                                    <div className="product text-center">
                                        <div className="mb-3 position-relative">
                                            <div className="badge text-white badge-"></div>
                                            <a className="d-block" href="detail.html">
                                                <img src={each.image} alt={each.title} width="255px" height="280px"/>
                                            </a>
                                            <div className="product-overlay">
                                                <ul className="mb-0 list-inline">
                                                    <li className="list-inline-item m-0 p-0"><a className="btn btn-sm btn-outline-dark" href="cart.html"><i className="far fa-heart"></i></a></li>
                                                    <li className="list-inline-item m-0 p-0"><a className="btn btn-sm btn-dark" href="cart.html">Add to cart</a></li>
                                                    <li className="list-inline-item mr-0"><a className="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i className="fas fa-expand"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <h6 className="col-12 text-truncate"> <a className="reset-anchor" href="detail.html">{each.title}</a></h6>
                                        <p className="small text-muted">${each.price}</p>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                : <div>Loading...</div>
                }
            </div>
        )
    }
}

export default Products