import React from 'react';

class Products extends React.Component {
    state = {
        products : null
    }

    async componentDidMount() {
        const response = await fetch('https://fakestoreapi.com/products');
        const data = await response.json();
        this.setState({
            products: data
        });
    }
    
    render() {
        const {products} = {...this.state};
        console.log(products);
        return(
            <div>
                {products && products.length > 0 ? 
                    <div>
                        {products.map(each => <div>{each.title}</div>)}
                    </div>
                : <div>Loading...</div>
                }
            </div>
        )
    }
}

export default Products