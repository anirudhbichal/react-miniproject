import React from 'react'

className Filter extends React.Component {
    render() {
        return(
            <div className="col-lg-3 order-2 order-lg-1">
                <h5 className="text-uppercase mb-4">Categories</h5>
                <div className="py-2 px-4 bg-dark text-white mb-3"><strong className="small text-uppercase font-weight-bold">Fashion &amp; Acc</strong></div>
                <ul className="list-unstyled small text-muted pl-lg-4 font-weight-normal">
                    <li className="mb-2"><a className="reset-anchor" href="#">Women's T-Shirts</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Men's T-Shirts</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Dresses</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Novelty socks</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Women's sunglasses</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Men's sunglasses</a></li>
                </ul>
                <div className="py-2 px-4 bg-light mb-3"><strong className="small text-uppercase font-weight-bold">Health &amp; Beauty</strong></div>
                <ul className="list-unstyled small text-muted pl-lg-4 font-weight-normal">
                    <li className="mb-2"><a className="reset-anchor" href="#">Shavers</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">bags</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Cosmetic</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Nail Art</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Skin Masks &amp; Peels</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Korean cosmetics</a></li>
                </ul>
                <div className="py-2 px-4 bg-light mb-3"><strong className="small text-uppercase font-weight-bold">Electronics</strong></div>
                <ul className="list-unstyled small text-muted pl-lg-4 font-weight-normal mb-5">
                    <li className="mb-2"><a className="reset-anchor" href="#">Electronics</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">USB Flash drives</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Headphones</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Portable speakers</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Cell Phone bluetooth headsets</a></li>
                    <li className="mb-2"><a className="reset-anchor" href="#">Keyboards</a></li>
                </ul>
            </div>
        )
    }
}

export default Filter