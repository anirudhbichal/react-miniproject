import React from 'react';

const User = (props) => {
    return (
        <>
            {props.user ? <span><img className="user-icon" src={props.user.picture.thumbnail} alt="user-icon" /> {props.user.name.first} {props.user.name.last}</span> : <span><i className="fas fa-user-alt mr-1 text-gray"></i>Login</span>}
        </>
    )
}

export default User