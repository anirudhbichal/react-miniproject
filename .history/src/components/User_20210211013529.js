import React from 'react';

class User extends React.Component {

    async componentDidMount() {
        const response = await fetch("https://randomuser.me/api");
        console.info(response.json());
    }

    render() {
        return (
            <div><span>User Icon</span> User Name</div>
        )
    }
}

export default User