import React from 'react';

class Products extends React.Component {
    state = {
        products : null
    }

    async componentDidMount() {
        const response = await fetch('https://fakestoreapi.com/products');
        const data = await response.json();
        this.setState({
            products: data
        });
    }
    
    render() {
        const {products} = {...this.state};
        return(
            <div className="col-lg-9 order-1 order-lg-2 mb-5 mb-lg-0">
                <div className="row mb-3 align-items-center">
                    <div className="col-lg-6 mb-2 mb-lg-0">
                        <p className="text-small text-muted mb-0">Showing 1–12 of 53 results</p>
                    </div>
                </div>
                {products && products.length > 0 ? 
                    <div className="row">
                        {products.map((each, index) => {
                            return (<div className="card col-3 p-2" key={index}>
                                <img className="card-img-top" src={each.image} alt={each.title} width="350px" height="350px" />
                                <div className="card-body text-left">
                                    <h5 className="card-title">{each.title}</h5>
                                    <h5 className="card-text">$ {each.price}</h5>
                                </div>
                            </div>)
                        })}
                    </div>
                : <div>Loading...</div>
                }
            </div>
        )
    }
}

export default Products