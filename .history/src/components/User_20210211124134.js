import React from 'react';

class User extends React.Component {
    state = {
        user: null
    };

    componentDidMount() {
        fetch("https://randomuser.me/api")
            .then(res => res.json())
            .then(data => {
                this.setState({
                    user: data.results[0]
                });
            });
    }

    render() {
        return (
            <div>
                <div><img className="user-icon" src={this.state.user.picture.thumbnail} alt="user-icon" /> {this.state.user.name.first} {this.state.user.name.last}</div>
            </div>
        )
    }
}

export default User