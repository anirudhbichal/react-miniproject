import React from 'react';

class Products extends React.Component {
    state = {
        products : null
    }

    async componentDidMount() {
        const response = await fetch('https://fakestoreapi.com/products');
        const data = await response.json();
        this.setState({
            products: data
        });
    }
    
    render() {
        const {products} = {...this.state};
        return(
            <div>
                {products && products.length > 0 ? 
                    <div className="d-flex flex-row justify-content-start">
                        {products.map((each, index) => {
                            return (<div className="card" key={index}>
                                <img className="card-img-top" src={each.image} alt={each.title} />
                                <div className="card-body">
                                    <h5 className="card-title">{each.title}</h5>
                                    <p className="card-text">{each.description}</p>
                                </div>
                            </div>)
                        })}
                    </div>
                : <div>Loading...</div>
                }
            </div>
        )
    }
}

export default Products