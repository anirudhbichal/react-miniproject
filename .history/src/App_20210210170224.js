import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3>React Search and Filter Application</h3>
      </header>
    </div>
  );
}

export default App;
