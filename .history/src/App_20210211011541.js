import './App.css';
import User from './components/User';

function App() {
  return (
    <div className="App container-fluid">
      <header className="App-header">
        <h2 className="d-flex justify-content-start">React Search and Filter Application</h2>
        <div className="user-info d-flex justify-content-end">
          <User />
        </div>
      </header>
    </div>
  );
}

export default App;
