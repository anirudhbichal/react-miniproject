import './App.css';
import User from './components/User';

function App() {
  return (
    <div className="App">
      <header className="App-header d-flex">
        <div className="d-flex">
          <h2>React Search and Filter Application</h2>
        </div>
        <div className="d-flex user-info ml-auto">
          <h2>
            <User />
          </h2>
        </div>
      </header>
    </div>
  );
}

export default App;
