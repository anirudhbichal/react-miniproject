import './App.css';
import User from './components/User';
import logo from './logo.svg';

function App() {
  return (
    <div className="App">
      <header className="App-header d-flex align-items-center">
        <div className="d-flex align-items-center">
          <h2><img src={logo} alt="logo" width="60px" height="60px" /> E-Comm</h2>
        </div>
        <div className="d-flex user-info ml-auto">
          <h2>
            <User />
          </h2>
        </div>
      </header>
    </div>
  );
}

export default App;
